#!/usr/bin/env python3

'''
Main entry point.
'''

import sys

from . import main

if __name__ == '__main__':
    sys.exit(main())
