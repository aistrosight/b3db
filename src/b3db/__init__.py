#!/usr/bin/env python3

'''
Module for working with B3DB data: https://github.com/theochem/B3DB
'''

import argparse
import logging
import os
import pathlib
import sys

import pandas as pd


LOGGER = logging.getLogger(__name__)


class B3DBException(Exception):
    '''
    Base exception raised by this module.
    '''


class Loader():
    '''
    Load B3DB data as Pandas dataframes.
    '''
    def __init__(self, path=None):
        '''
        Args:
            path:
                The path to the B3DB directory. If this is not set, the value of
                the environment variable B3DB_DIR will be used.
        '''
        if path is None:
            path = os.environ.get('B3DB_DIR')
        if path is None:
            raise B3DBException(
                'B3DB directory path must be passed as an argument '
                'or via the environment variable B3BD_DIR'
            )
        self.path = pathlib.Path(path)

    def load_file(self, name, **kwargs):
        '''
        Load and return a file from the current B3DB directory.

        Args:
            name:
                The name of the file in the B3DB directory to load.

            **kwargs:
                Keyword arguments passed through to read_csv().

        Returns:
            A Pandas dataframe with the selected data.

        Raise:
            B3DBException:
                The file could not be loaded due to an OSError.
        '''
        path = self.path / name
        LOGGER.info('loading %s', path)
        try:
            return pd.read_csv(path, sep='\t', **kwargs)
        except OSError as err:
            raise B3DBException(f'failed to load {path}: {err}') from err

    def get_classification_data(self):
        '''
        Get the classification data.

        Returns:
            The classification data in a Pandas dataframe.

        Raises:
            See load_file().
        '''
        return self.load_file('B3DB_classification.tsv')

    def get_extended_classification_data(self):
        '''
        Get the extended classification data.

        Returns:
            The extended classification data in a Pandas dataframe.

        Raises:
            See load_file().
        '''
        return self.load_file('B3DB_classification_extended.tsv.gz', low_memory=False)

    def get_regression_data(self):
        '''
        Get the regression data.

        Returns:
            The regression data in a Pandas dataframe.

        Raises:
            See load_file().
        '''
        return self.load_file('B3DB_regression.tsv')

    def get_extended_regression_data(self):
        '''
        Get the extended regression data.

        Returns:
            The extended regression data in a Pandas dataframe.

        Raises:
            See load_file().
        '''
        return self.load_file('B3DB_regression_extended.tsv.gz', low_memory=False)


def main(args=None):
    '''
    Query table information.

    Args:
        args:
            A list of arguments to parse. If None, the command-line arguments are used.

    Raises:
        See load_file().
    '''
    # Map command-line options to descriptions and methos.
    table_meta = {
        'cls': ('classification data', 'get_classification_data'),
        'cls_ext': ('extended classification data', 'get_extended_classification_data'),
        'reg': ('regression data', 'get_regression_data'),
        'reg_ext': ('extended regression data', 'get_extended_regression_data'),
    }
    table_choices = sorted(table_meta)
    table_help = '; '.join(f'{name}: {table_meta[name][0]}' for name in table_choices)

    parser = argparse.ArgumentParser(description='Get B3DB table information.')
    parser.add_argument(
        'table',
        choices=table_choices,
        help=f'Display informaton for this table. {table_help}'
    )
    parser.add_argument(
        '-d', '--dir',
        help=(
            'The path to the directory containing the B3DB TSV files. '
            'If not given, the path must be set in the environment variable '
            'B3DB_DIR.'
        )
    )
    pargs = parser.parse_args(args=args)

    name = pargs.table
    method = table_meta[name][1]
    try:
        loader = Loader(path=pargs.dir)
        table = getattr(loader, method)()
    except B3DBException as err:
        return str(err)
    table.info()
    return 0


if __name__ == '__main__':
    sys.exit(main())
