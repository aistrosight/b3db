# README

## About

The `b3db` module provides a simple class named `Loader` for loading data from the [Blood-Brain Barrier Database (B3DB)](https://github.com/theochem/B3DB).

## Usage

The loader will require a path to a directory containing the B3DB data files (see the "Configuration" section below). Once configured, import the `Loader` class and use it to retrieve the desired B3DB data as a Pandas Dataframe:

### Example

~~~python
from b3db import Loader
loader = Loader()
classification_data = loader.get_classification_data()
~~~


### Documentation

The Sphinx-generated online documentation can be found [here](https://aistrosight.gitlabpages.inria.fr/b3db/).


## Configuration

The B3DB data files must be available locally. Clone the B3DB Git repository into a local directory and get the absolute path to the B3DB subdirectory containing the TSV files:

~~~sh
git clone https://github.com/theochem/B3DB.git
readlink -f ./B3DB/B3DB
~~~

The path, hereafter referred to as `<data dir>`, can either be passed to the `Loader` class when it is initialized, e.g.

~~~python
loader = Loader(path="<data dir>")
~~~

or it can be passed via the environment variable `B3DB_DIR`, in which case the `path` parameter can be omitted:

~~~python
loader = Loader()
~~~

The path parameter takes preference over the environment variable. To configure the environment variable for the current session, run the following command:

~~~sh
export B3DB_DIR=<data dir>
~~~

The command can also be added to a file such as `~/.bashrc` for a persistent configuration or to a separate environment variable file that is sourced for each session. For example, create the file `env.sh` with the following contents:

~~~sh
#!/usr/bin/sh
export B3DB_DIR=<data dir>
~~~

and then source it with `source env.sh` to configure your session.


## Table Information Utility

The module can also be used from the command-line to query table information. For example:

~~~sh
> python3 -m b3db cls
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 7807 entries, 0 to 7806
Data columns (total 12 columns):
 #   Column         Non-Null Count  Dtype
---  ------         --------------  -----
 0   NO.            7807 non-null   int64
 1   compound_name  6698 non-null   object
 2   IUPAC_name     6170 non-null   object
 3   SMILES         7807 non-null   object
 4   CID            6170 non-null   float64
 5   logBB          1058 non-null   float64
 6   BBB+/BBB-      7807 non-null   object
 7   Inchi          7807 non-null   object
 8   threshold      3621 non-null   float64
 9   reference      7807 non-null   object
 10  group          7807 non-null   object
 11  comments       18 non-null     object
dtypes: float64(3), int64(1), object(8)
memory usage: 732.0+ KB
~~~

See `python3 -m b3db --help` for more information. If the module is not installed on your Python path, it can also be invoked with `python3 /path/to/b3db.py [options...]`.
