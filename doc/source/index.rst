`b3db <https://gitlab.inria.fr/aistrosight/b3db>`_ Documentation
================================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    readme
    modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
